# PyLab

Ce dépôt contient une correction pour le projet de programmation sur la génération et visite de labyrinthe. Il y a 3 fichiers principaux: 
+ `solution_generate.py` pour générer un labyrinthe,
+ `solution_shortest.py` pour calculer un plus court chemin,
+ `solution_visit.py` pour faire des *visites* (sans visibilité du labyrinthe complet).

Chacun de ces fichiers peut être exécuté en ligne de commande, et
l'option `-h` ou `--help` donne les options utilisables. Par exemple, 

`python3 solution_generate.py --help`

affiche

```
usage: Generate a labyrinth of given size [-h] [-o FILE] [--slow] [-s]
                                          [-d DRAW] [-r X]
                                          n m

positional arguments:
  n                     Number of columns of the labyrinth
  m                     Number of rows of the labyrinth

optional arguments:
  -h, --help            show this help message and exit
  -o FILE               File to save the result
  --slow                Use the slow algorithm for checking connected
                        components
  -s, --show            Show the result in compact form
  -d DRAW, --draw DRAW  Draw the result in an image with this filename
  -r X                  Remove X random walls from the labyrinth
```

L'option de dessiner dans un fichier nécessite `wand` (basé sur
ImageMagick, plus d'informations
[sur leur page](https://docs.wand-py.org/)). L'affichage des visites
dans le terminal n'est pas garanti de marcher sous Windows.


# Sujet du Projet de Programmation

Le projet de cette année portera sur les labyrinthes, sur une grille rectangulaire. Il faudra développer trois fonctionnalités:

+ générer un labyrinthe aléatoire
+ calculer un chemin le plus court possible jusqu'à la sortie
+ faire une visite du labyrinthe pour atteindre la sortie

Le langage préféré est Python. L'utilisation d'un autre langage de programmation est possible, mais consultez-moi avant de commencer. Bien entendu, des implémentations de ces fonctionnalités sont disponibles, mais le but du projet de programmation est de développer des implémentations originales. Il est nécessaire que vous soyez capables d'expliquer votre code et les algorithmes qu'il contient. Il est fortement conseillé d'utiliser une plate-forme de développement collaboratif, même si vos codez tout seul (un dépôt Git par exemple, https://gitlab.com peut vous le fournir).

## Il est demandé de fournir:

+ Le code complet de votre projet (avec tout le nécessaire pour compiler / exécuter), sous forme d'un seul fichier d'archive.
+ Une documentation utilisateur expliquant comment l'utiliser.
+ Une documentation développeur expliquant comment le code est structuré, dans quel(s) fichier(s) se trouvent chaque fonctionnalité, et un résumé des algorithmes utilisés.

## Critères utilisés pour la notation:

+ Régularité dans l'avancement du projet
+ Qualité du code (organisation, réutilisabilité)
+ Accessibilité et lisibilité du code (commentaires, noms de variables et de fonctions)
+ Qualité de la documentation (clarté, longueur appropriée, facilité de lecture, exactitude des informations)
+ Efficacité du code (pertinence des structures de données, complexité des algorithmes, temps de calcul)
+ Ingéniosité et originalité des algorithmes utilisés
+ Quantité / Difficulté des fonctionnalités implémentées

## Génération aléatoire

On se place sur une grille rectangulaire de taille n par m. Un labyrinthe est dit parfait s'il existe un et un seul chemin pour aller d'une case à une autre. Il existe de nombreux algorithmes pour générer un labyrinthe parfait, chaque algorithme génère des labyrinthes avec une structure particulière. Un certain Jamis Buck en a fait [une liste assez longue](http://weblog.jamisbuck.org/2011/2/7/maze-generation-algorithm-recap.html) dont vous pouvez vous inspirer.

**Travail demandé:** implémenter au moins deux algorithmes de génération différents. Écrire un programme qui prend en entrée les dimensions (n et m) du labyrinthe, et qui sauvegarde le résultat dans un fichier au format décrit ci-dessous. Estimer la complexité des algorithmes implémentés, et tester leur temps d'exécution quand la taille du labyrinthe varie.

**Format de fichier:** Le labyrinthe sera décrit avec la liste des murs verticaux et des murs horizontaux. La première ligne du fichier donnera la taille du labyrinthe, sous la forme

`n m`

Chaque ligne suivante du fichier contiendra un mur, sous la forme

`x y H`

pour un mur horizontal (cela indique la présence d'un mur entre les cases (x, y) et (x, y+1), ou

`x y V`

pour un mur vertical (qui indique la présence d'un mur entre les cases (x, y) et (x+1, y). Les cases sont numérotées de 0 à n-1 pour la première coordonnée, de 0 à m-1 pour la deuxième. Excepté la première, l'ordre des lignes dans le fichier peut être quelconque.

## Plus court chemin

Si on peut regarder l'ensemble du labyrinthe avant de partir, comment calculer le plus court chemin jusqu'à la sortie ? On suppose que l'entrée du labyrinthe se fait sur la case (0, 0), et la sortie sur la case (n-1, m-1). Comme chaque pas dans le labyrinthe compte pour une distance de 1, il est possible de calculer ce plus court chemin avec un parcours en largeur.

**Travail demandé:** Écrire un programme qui lit un fichier au format décrit ci-dessus, et qui écrit la longueur du plus court chemin dans le labyrinthe. Tester le programme sur différents labyrinthe, parfaits ou non.

## Visite

On suppose maintenant qu'on se trouve à l'intérieur du labyrinthe, et qu'à chaque instant on ne peut voir que les murs et les cases autour de soi. L'objectif est toujours de trouver la sortie en un minimum de déplacements. Une visite bien connue est celle de la main gauche, qui consiste à toujours suivre le mur à sa gauche ; dans certaines conditions cette visite trouve toujours la sortie.

**Travail demandé:** Écrire un programme qui lit un fichier au format décrit ci-dessus, et qui visite le labyrinthe pour trouver la sortie en ne voyant que les cases voisines autour de la case où on se trouve (mais il est possible de se souvenir de ce qu'on a vu jusque là). Le programme doit écrire la longueur du chemin parcouru jusqu'à la sortie. Essayer d'améliorer l'algorithme de visite pour utiliser le moins de mouvements possibles.

**Et ensuite:** Tester ce programme sur plusieurs labyrinthes générés par le premier programme. Comparer la longueur de la visite et la longueur du plus court chemin. Essayer de modifier les algorithmes de génération aléatoire pour fabriquer des labyrinthes "plus difficiles".

## Comparaison finale

Après le rendu final, vos algorithmes seront comparés en faisant un "tournoi" : pour chaque paire d'étudiant⋅e⋅s A et B, 20 labyrinthes seront générés par l'algorithme de génération de A, et visités par l'algorithme de visite de B. L'efficacité sera jugée sur le rapport moyen entre la  longueur de la visite et le plus court chemin pour ce labyrinthe. L'étudiant⋅e A cherchera à augmenter ce rapport, l'étudiant⋅e B cherchera à le faire baisser. 
